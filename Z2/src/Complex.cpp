#include <iostream>
#include <math.h>
#include "Complex.hh"
#include "Expression.hh"
#include "Statistics.hh"
#include <limits>
/*!
 * Realizuje dodanie dwoch liczb zespolonych.
 * Argumenty:
 *    arg1 - pierwszy skladnik dodawania,
 *    arg2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */
Complex operator+(Complex arg1, Complex arg2)
{
    Complex result;

    result.re = arg1.re + arg2.re;
    result.im = arg1.im + arg2.im;
    result.br2  = arg1.br2;
    result.br1 = arg1.br1;
    result.i = arg1.i;
    return result;
}

/*! 
 * Realizuje odejmowanie dwoch liczb zespolonych.
 * Argumenty:
 *    arg1 - pierwszy skladnik odejmowania,
 *    arg2 - drugi skladnik odejmowania.
 * Zwraca:
 *    Roznica dwoch skladnikow przekazanych jako parametry.
 */
Complex operator-(Complex arg1, Complex arg2){
    
    Complex result;

    result.re = arg1.re - arg2.re;
    result.im = arg1.im - arg2.im;
    result.br2 = arg1.br2;
    result.br1 = arg1.br1;
    result.i = arg1.i;
    return result;
}

/*!
 * Realizuje mnoznie dwoch liczb zespolonych.
 * Argumenty:
 *    arg1 - pierwszy skladnik mnoznia,
 *    arg2 - drugi skladnik mnozenia.
 * Zwraca:
 *    Iloczyn dwoch skladnikow przekazanych jako parametry.
 */
Complex operator*(Complex arg1, Complex arg2){
    
    Complex result;

    result.re = arg1.re*arg2.re - arg1.im*arg2.im;
    result.im = arg1.re*arg2.im + arg1.im*arg2.re;
    result.br2 = arg1.br2;
    result.br1 = arg1.br1;
    result.i = arg1.i;
    return result;
}

/*!
 * Realizuje dzielenie dwoch liczb zespolonych.
 * Argumenty:
 *    arg1 - pierwszy skladnik dzielenia,
 *    arg2 - drugi skladnik dzielenia.
 * Zwraca:
 *    Iloraz dwoch skladnikow przekazanych jako parametry.
 */
Complex operator/(Complex arg1, Complex arg2){
    
    Complex result;
    double den;
    arg2 = conjugate(arg2);
    result = arg1 * arg2;
    den = pow(Mod(arg2),2);
    result.re = result.re/den;
    result.im = result.im/den; 
    return result;  
}

/*!
* Funckja sprzezajaca wyrazenie
* Argument :
*   arg - liczba zepolona
* Zwraca:
*   Sprzezona liczbe zespolona
*/
Complex conjugate(Complex  arg)
{
        arg.im = -1 * arg.im;
        return arg;
}

/*!
* Modul liczby zepolonej.
* Argument
*   arg - liczba zespolona,
* Zwraca 
*   modul z liczby zespolonej.
*/
double Mod(Complex arg)
{
    return sqrt(arg.re*arg.re + arg.im * arg.im);
}

/*!
 * Realizuje wczytanie liczby zespolonej.
 * Argumenty:
 *    in  - strumien wejsciowy,
 *    arg1 - wczytana liczba zespolona.
 * Zwraca:
 *    Strumien wejsciwy wczytujacy liczbe zespolona
 */
std::istream & operator>>(std::istream &in, Complex & arg1)
{   //char br1,br2,i;
    in >> arg1.br1  >> arg1.re >> arg1.im >> arg1.i >> arg1.br2;
    
    return in;
}


bool digit()
{
    return false;
}


/*!
 * Realizuje wyswitlenie liczby zespolonej.
 * Argumenty:
 *    out - strumien wyjsciowy,
 *    arg1 - wyswitlana liczba zespolona.
 * Zwraca:
 *    Strumien wyjscowy wyswietlajacy liczbe zespolona.
 */
std::ostream & operator<<(std::ostream &out, Complex & arg1)
{
    out << arg1.br1 << arg1.re << std::showpos << arg1.im << std::noshowpos << arg1.i << arg1.br2;
    return out;
}
 
/*!
 * Realizuje wczytanie operatora.
 * Argumenty:
 *    in - strumien wejsciowy,
 *    Op - operator.
 * Zwraca:
 *    Strumien wejscowy wczytujacy operator.
 */
std::istream & operator >> ( std::istream & in, Operator & Op )
{
    char CzytSymOp = 'x';
    in >> CzytSymOp;
    switch(CzytSymOp){
        case '+': 
            Op = kAddition; break;
        case '-': 
            Op = kSubtraction; break;
        case '*': 
            Op = kMultiplication; break;
        case '/': 
            Op = kDivision; break;
        default: Op = Error;
            return in; 
            break;
    }
    return in;
}

/*!
 * Realizuje wyswietlenie operatora.
 * Argumenty:
 *    out - strumien wyjsciowy,
 *    Op - operator.
 * Zwraca:
 *    Strumien wyjsciowy wyswietlajacy operator.
 */
std::ostream & operator << (std::ostream & out, Operator & Op )
{
    switch (Op){
        case kAddition: out << '+';  break;
        case kSubtraction: out << '-'; break;
        case kMultiplication: out << '*'; break;
        case kDivision: out << '/'; break;
        default: break;
    }
    return out;
}

//MODYFIKACCJE 

//WYWŁANIE np. arg1 = arg1+=arg2;
Complex operator+=(Complex  arg1, Complex arg2){
    arg1 = arg1 +arg2;
    return arg1;
}
//WYWŁANIE np. arg1 = arg1*=arg2;
Complex operator*=(Complex  arg1, Complex arg2){
    arg1 = arg1 * arg2;
    return arg1;
}
//funkcja obliczbajac wartosc kata
double angle(Complex arg){
    double x,y,z;
    y = arg.re/arg.im;
    if(arg.re!= 0)
    {
        if(arg.re >0)
        {   
            x = atan(y) * 180/3.14;
        }
        else{
            x = atan(y)*180/3.14 + 3,14;
        }
    }
    else{
        if(arg.im > 0)
            x = 90;
        else
            x = 270;
    }
    if(x<0)
        return 180 - x;
    else 
        return x;
}