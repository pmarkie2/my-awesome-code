#include <iostream>
#include "Statistics.hh"
#include "Complex.hh"
#include <iomanip>
using namespace std;


/*!
 * Fukcja wyswietlajaca kocowe statystyki testu.
 * Argumenty: 
 *    stat - struktura zwierajaca statystyki testu.
 * Zwraca:
 *    komunikat o wyniku porcentowym dobrych odpowiedzi,
 *    komunikat kiedy nie ma pytan na ktore urzytkownik
 *      moglby odpowiedziec.
 */ 
void display( Statistics  stat)
{
    cout<<endl<<" Poprawne odp: "<<stat.good<<endl;
    cout<<" Bledne odp: "<<stat.bad<<endl;

    double result;
    if(stat.nr_question != 0){
      result = ((double)stat.good/(double)stat.nr_question)*100;
      cout<<" Wynik procentowy poprawnych odp: "<< setprecision(4) << result <<"%"<<endl;
    }
    else
    {
      cout << " Brak poprawnych pytan, brak wyniku!" << endl;
    }
    
}

/*!
 * Funkcja sprawdzajac czy wynik operacji + - * / jest prawidlowy.
 * Argumenty:
 *    result - wskaznik na prawidlowa odpwiedz,
 *    user_result - wskaznik na odpwiedz uzytkownika.
 * Zwraca:
 *    true - odpwiedz uzytkownika jest poprawna,
 *    false - jesli jest bledna.
 */
 
bool check(Complex *result, Complex *user_result)
{
  
  if( result->re == user_result->re && result->im == user_result->im)
  {
    cout<<"Poprawna odp." << endl << endl;
    return true;
  }
  else
  {  
    cout<<endl<<"Błędna odp. Poprawna -> "<<*result<<endl<<endl;
    return false;
  }
}