#include <iostream>
#include <cstring>
#include <cassert>
#include <fstream>
#include "Database.hh"

using namespace std;

/*
 * W bazie testu ustawia wybrany test jako biezacy test i indeks pytania
 * ustawia na pierwsze z nich.
 * Parametry:
 *    database_ptr        - wskaznik na zmienna reprezentujaca baze testu,
 *    number_of_questions - ilosc pytan w tablicy.
 *   
 * Warunki wstepne:
 *      - Parametr database_ptr nie moze byc pustym wskaznikiem. Musi zawierac adres
 *        zmiennej reprezentujacej baze testu, ktora wczesniej zostal poprawnie
 *        zainicjalizowany poprzez wywolanie funkcji Init.
 *      - Parametr number_of_questions zawiera wartosc, ktora nie przekracza ilosci elementow
 *        w pliku.
 */
void SetTest(Database *database_ptr, unsigned int number_of_questions)
{
    database_ptr->number_of_questions = number_of_questions;
    database_ptr->question_index = 0;
}

/*
 * Inicjalizuje test kojarzac zmienna dostepna poprzez wskaznik database_ptr
 * z dana tablica wyrazen, ktora reprezentuje jeden z dwoch dopuszczalnych 
 * testow.
 * Parametry:
 *    database_ptr - wskaznik na zmienna reprezentujaca baze testu.
 *    test_name  - wskaznik na napis wybranego typu testu.
 *
 * Warunki wstepne:
 *      - Parametr database_ptr nie moze byc pustym wskaznikiem. Musi zawierac adres
 *        zmiennej reprezentujacej baze testu, ktora wczesniej zostal poprawnie
 *        zainicjalizowany poprzez wywolanie funkcji Init.
 *      - Parametr test_name musi wskazywac na jedna z nazw tzn. "latwe" lub "trudne".
 *       
 * Zwraca:
 *       true - gdy operacja sie powiedzie i test zostanie poprawnie
 *              zainicjalizowany,
 *       false - w przypadku przeciwnym.
 */
bool Init(Database *database_ptr, const char *test_name)
{
    if (!strcmp(test_name, "latwy"))
    {
        SetTest(database_ptr, question_nr(test_name));
        database_ptr->test_name = "latwy"; 
        return true;
    }

    else if(!strcmp(test_name, "trudny"))
    {
        SetTest(database_ptr, question_nr(test_name));
        database_ptr->test_name = "trudny";
        return true;
    }

    cerr << "Otwarcie testu '" << test_name << "' nie powiodlo sie." << endl;
    return false;
}

/*!
 * Funkcja zwraca ilosc wyrazen.
 * Argumenty: 
 *   test_name - nazwa testu latwy/trudny.
 * Zwraca:
 *   number_of_questions - ilosc wyrazen zepspolonych.
 */
int question_nr(string test_name){

    int number_of_questions = 0;
    if(test_name == "latwy")
    {
        number_of_questions =  easy();
        return number_of_questions;
    }
    else
    {
        number_of_questions = hard();
        return number_of_questions;
    }
}

/*!
 * Fukcja zwracajac ilosc wyrazen.
 * Brak parametrow wstepnych.
 * Brak argumentow.
 * Zwraca:
 *   nr_lini - ilosc wyrazen w pliku.
 */
int easy(){
    Database base;
    ifstream plik;
    plik.open(base.easy);
    string line;
    int nr_line = 0, empty_line = 0;
    while(true){
        getline(plik, line);
        if(line == ".")
            break;
        if(line.empty() == true)
            empty_line++;
        if(line.empty() == true && plik.peek() == EOF){
            nr_line++;
            break;
        }
        nr_line++;
    }
    plik.close();
    return nr_line - empty_line;
}

/*!
 * Fukcja zwracajac ilosc wyrazen.
 * Brak parametrow wstepnych.
 * Brak argumentow.
 * Zwraca:
 *   nr_lini - ilosc wyrazen w pliku.
 */
int hard(){
    Database base;
    ifstream plik;
    plik.open(base.hard);
    string line;
    int nr_line = 0, empty_line = 0;
    while(true){
        getline(plik,line);
        if( line == ".")
            break;
        if(line.empty() == true )
            empty_line++;
        if(line.empty() && plik.peek() == EOF){   
            nr_line++;
            break;
        }
        nr_line++;
    }
    plik.close();
    return nr_line - empty_line;
}


/*!
 *  Fukcja sprawdzajaca poprawnosc wprowadzonego wyrazenia.
 *  Argumenty:
 *      expr - wskaznik na wyrazenie zepspolone.
 *  Zwraca:
 *      true - jesli wyrazenie jest poprawne,
 *      false - jesli bedne.
 */
bool expr_check(Expression *expr){       
    if(answer_check(&expr->arg1) == false ||  answer_check(&expr->arg2) == false || expr->op == Error)
    {
        return false;
    }
    else 
        return true;
}

/*!
 *  Fukcja sprawdzajaca czy arg2 w wyrazeniu zespolonym nie jest rowny 0.
 *  Argumenty:
 *      expr - wskaznik na wyrazenie zepspolone.
 *  Zwraca:
 *      true - jesli arg2 jest rozny od 0,
 *      false - jesli arg2 jest rowny 0.
 */
bool Zero(Expression *expr){
    if(expr->arg2.re == 0 && expr->arg2.im == 0 && expr->op == kDivision )
        return false;
    else
        return true;
}

/*!
 *  Fukcja sprawdzajaca poprawnosc liczby zespolonej.
 *  Argumenty:
 *      answer - wskaznik na liczbe zespolona.
 *  Zwraca:
 *      true - jesli liczba jest zapisane poprawnie,
 *      false - jesli bednnie.
 */
bool answer_check(Complex *answer){
    if(answer->br1 == '(' && answer->br2 == ')' && answer->i == 'i')
        return true;
    else
        return false;
}