#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <limits>
#include "Database.hh"
#include "Complex.hh"
#include "Expression.hh"
#include "Statistics.hh"

using namespace std;

int main(int argc, char **argv)
{
    
    if (argc < 2)
    {
        std::cout << endl;
        std::cout << " Brak opcji okreslajacej rodzaj testu." << endl;
        std::cout << " Dopuszczalne nazwy to:  latwy, trudny." << endl;
        std::cout << endl;
        return 1;
    }
    
    Database base;

    if (Init(&base, argv[1]) == false)
    {
        cerr << " Inicjalizacja testu nie powiodla sie." << endl;
        return 1;
    }

    std::cout << endl;
    std::cout << " Start testu arytmetyki zespolonej: " << argv[1] << endl;
    std::cout << endl;

    Expression expr;
    Complex result, user_result;
    Statistics stat;
    ifstream plik;

    if(base.test_name == "latwy")
        plik.open(base.easy);
    else
        plik.open(base.hard);
    
    while (base.number_of_questions > base.question_index)
    {   
        
        if(!(plik>>expr)){
            std::cout << " Bledne wyrazenie, pomijam!" << endl << endl;
            plik.clear();
            plik.ignore(numeric_limits<streamsize>::max(),'\n');
        }
        else{
            if(Zero(&expr) == false)
                cout<<" Nie dzili sie przez zero! Pomijam!" <<endl;
            else if (expr_check(&expr) == true)
            {
                stat.nr_question++;
                std::cout<< stat.nr_question << ". Podaj wynik operacji: "; Display(expr); std::cout << endl; result = Solve(&expr);
                int attempt = 0, attempts_left = 3;
                while(attempt < 3)
                {
                    std::cout<< " Twoja odpowiedz: "; 
                    if(!(cin>>user_result) || answer_check(&user_result) == false){                                                           
                        attempt++; 
                        attempts_left--;
                    
                        if(attempts_left == 0 && attempt == 3){
                            std::cout<<"Brak prob, bledna odp."<<endl;
                            stat.bad++;
                        }
                        else{
                            std::cout<<" Zla konstrukcja liczby zepspolonej!";
                            std::cout<<" Pozostalo "<< attempts_left << " !" << endl;
                        }
                        cin.clear();
                        cin.ignore(numeric_limits<streamsize>::max(),'\n');
                    }
                    else {
                        if( check(&result, &user_result)== true){
                            stat.good++; break;
                        }
                        else{ 
                            stat.bad++; break;
                        }
                    }
                }
            }
        }
        base.question_index++;
    }
    plik.close();
    display(stat);
    std::cout << endl;
    std::cout << "Koniec testu" << endl;
    std::cout << endl;
}

