#include <iostream>
#include "Complex.hh"
#include "Expression.hh"

/*!
* Funkcja wyswitlajaca wyrazenie zespolone
* Argumenty:
*   expr - wyrazenie zespolone
*/
void Display(Expression expr){
    std::cout<<expr;
}

/*!
* Funkcja rozwiazująca wyrazenie zespolone
* Argumenty:
*   expr - wczytane wyrazenie zepolone
* Zwraca: 
*   w zależnosci od znaku operatora wyraznie zespolonego
*   zwraca wynik dodwania, odejmowania, mnozenia lub dzielenia
*/
Complex Solve(Expression *expr){
    Complex result;
        switch(expr->op){
            case kAddition:{                        // Dodawanie
                result  = expr->arg1 + expr->arg2;            
                return result; break;
            } 
            case kSubtraction:{                     // Odejmowanie
                result = expr->arg1 - expr->arg2;
                return result; break;
            }
            case kMultiplication: {                 //Mnozenie
                result = expr->arg1 * expr->arg2;
                return result; break;
            }
            case kDivision: {                       //Dzielenie
                result = expr->arg1 / expr->arg2;
                return result; break;
            }
            default: break;
        }
        return result;
}

/*!
 * Realizuje wczytanie wyrazenia zespolonego.
 * Argumenty:
 *    in - strumien wejsciowy 
 *    expr - wyrazenie zespolone
 * Zwraca:
 *    Strumien wejscowy wczytujacy wyrazenie zespolone
 */
std::ostream & operator << (std::ostream & out, Expression & expr){
    out << expr.arg1 << expr.op << expr.arg2 ;
    return out;
}

/*!
 * Realizuje wyswietlenie wyrazenia zespolonego.
 * Argumenty:
 *    out - strumien wyjsciowy 
 *    expr - wyrazenie zespolone
 * Zwraca:
 *    Strumien wyjsciowy wyswietlajacy wyrazenie zespolone
 */
std::istream &  operator>>(std::istream &in, Expression & expr){
    in >> expr.arg1 >> expr.op >> expr.arg2;
    return in;
}

