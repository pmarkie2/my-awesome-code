#pragma once

#include "Expression.hh"

/*
 * Modeluje pojecie o maksymalnej ilosci pytan, indeksu
 * nastepnego pytania, ktore ma byc pobrane jak rowniez 
 * nazwe testu.
 */
struct Database{
    unsigned int number_of_questions = 0;
    unsigned int question_index = 0;
    std::string test_name;
    std::string easy = "latwy.dat";
    std::string hard = "trudny.dat";
};

/*
 * Definicje funkcji.
 */
bool answer_check(Complex *answer);
bool expr_check(Expression *expr);
bool Zero(Expression *expr);
bool Init(Database *database_ptr, const char *test_name);
int question_nr(std::string test_name);
int easy();
int hard();