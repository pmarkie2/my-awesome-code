#pragma once
#include "Complex.hh"

/*!
 * Modeluje strukture Statistics w ktorej przechowywane sa
 * ilosc dobry, zlych i dobrych pytan.
 */
struct Statistics {
    int good = 0;
    int bad = 0;
    int nr_question = 0;
};

/*
 * Definicje funkcji.
 */

bool check(Complex *result, Complex *user_result);
void display( Statistics  stat );