#pragma once
#include <iostream>

/*!
 *  Plik zawiera definicje struktury Complex oraz zapowiedzi
 *  przeciazen operatorow arytmetycznych dzialajacych na tej 
 *  strukturze.
 */

/*!
 * Modeluje pojecie liczby zespolonej
 */
struct Complex
{
    double re; /*! Pole repezentuje czesc rzeczywista. */
    double im; /*! Pole repezentuje czesc urojona. */
    char i = 'x';   /*! Pole repezentuje i. */
    char br1 = 'x'; /*! Pole repezentuje nawias (. */
    char br2 = 'x'; /*! Pole repezentuje nawias ). */
};

/*
 * Definicje przeciazen operatorow i funkcji.
 */

Complex conjugate(Complex arg);
double Mod(Complex arg);
double angle(Complex arg);
Complex operator+(Complex arg1, Complex arg2); 
Complex operator-(Complex arg1, Complex arg2);
Complex operator*(Complex arg1, Complex arg2);
Complex operator/(Complex arg1, Complex arg2);
Complex operator+=(Complex arg1, Complex arg2);
Complex operator*=(Complex arg1, Complex arg2);
std::istream & operator>>(std::istream & in, Complex & arg1);
std::ostream & operator<<(std::ostream & out, Complex & arg1);