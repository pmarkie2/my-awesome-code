#pragma once
#include "Complex.hh"
#include <iostream>

 /*
 *  Plik zawiera definicje struktury Expression i enumeatora Operator
 *  oraz zapowiedzi przeciazen operatorow arytmetycznych dzialajacych na tej strukturze.
 */

/*!
 * Modeluje zbior operatorow arytmetycznych.
 */
enum Operator
{
    kAddition,
    kSubtraction,
    kMultiplication,
    kDivision,
    Error,
};

/*
 * Modeluje pojecie dwuargumentowego wyrazenia zespolonego.
 */ 
struct Expression
{
    Complex arg1; // Pierwszy argument wyrazenia arytmetycznego
    Operator op;  // Opertor wyrazenia arytmetycznego
    Complex arg2; // Drugi argument wyrazenia arytmetycznego
};

/*
* Definicje funkcji i przeciazen operatow.
*/
void Display(Expression expr); 
Complex Solve(Expression *expr);
std::istream & operator >> ( std::istream & in, Operator & Op );
std::ostream & operator << ( std::ostream & out, Operator & Op );
std::istream & operator >> ( std::istream & in, Expression & expr );
std::ostream & operator << ( std::ostream & out, Expression & expr );